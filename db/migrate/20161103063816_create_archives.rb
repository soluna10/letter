class CreateArchives < ActiveRecord::Migration[5.0]
  def change
    create_table :archives do |t|
      t.references :user, foreign_key: true # user_id  생성
      t.references :post, foreign_key: true # post_id  생성

      t.timestamps
    end
  end
end
