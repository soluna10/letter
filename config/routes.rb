Rails.application.routes.draw do
  get 'users/show'

  mount Ckeditor::Engine => '/ckeditor'
  
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" ,registrations: 'registrations' }
  
  resources :search, only: [:index]

  root 'login#welcome'
  
  resources :posts do
  	collection do
  		get :wear
  		get :eat
  		get :live
  		get :culture
      get :about
      get :search
  	end
    member do
      put :archive
      delete :delete_archive
    end
  end

  resources :users do
    collection do
      get :show
    end
  end



  post 'posts/wear'
  post 'posts/eat'
  post 'posts/live'
  post 'posts/culture'
  post 'posts/about'
  post 'posts/search'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
