class Post < ActiveRecord::Base
	#attr_accessible :body, :title
  	belongs_to :user
  	has_many :archives
  	has_many :archiving_users, :through => :archives, :source => :user
  	enum category: [ :wear, :eat, :live, :culture]
  	enum season: [ :spring, :summer, :autumn, :winter]
  	#multisearchable :against => [:body, :title]
  	include PgSearch
  	
  pg_search_scope :search, against: [:title, :content], :using => {:tsearch => { :prefix => true}}
  


	def self.perform_search(keyword)
		if keyword.present?
     then Post.search(keyword)
		else Post.all
		end.sort
	end
end
