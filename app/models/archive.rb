class Archive < ActiveRecord::Base
  belongs_to :user
  belongs_to :post, counter_cache: :users_count
end
