class SearchController < ApplicationController
 def index
   if params[:query].present?
     gifts = Post.search(params[:query])
   else
     @gifts = Post.all
   end
 end
end
