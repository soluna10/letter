class PostsController < ApplicationController
	before_action :authenticate_user!, only:[ :edit, :update, :destory]
 	before_action :set_post, only: [ :show, :edit, :update, :destroy, :archive,  :delete_archive]
 	

 	# def search
  #   	@posts = Post.search(params[:query])
  # 	end

	def index
		if params[:query].present?
		  @posts = Post.perform_search(params[:query])
		else
		  @posts = Post.all
		end
	end

 	def wear
 		@posts = Post.wear
 	end

 	def eat
 		@posts = Post.eat
 	end

 	def live
 		@posts = Post.live
 	end

 	def culture
 		@posts = Post.culture
 	end

 	def show
 	end
 
 	def new
 		@post = Post.new
 	end
 
 	def create
 		@post = Post.new(post_params)
 		@post.user = current_user
 		if @post.save
 			redirect_to @post
 		else
 			render 'new'
 		end
 	end
 
 	def update 
 		if @post.update(post_params)
 			redirect_to @post
 		else
 			render 'edit'
 		end
 	end
 
 	def destroy
 		@post.destroy
 		redirect_to posts_path
 	end

 	# 좋아요 기능 추가
 	def archive
 		Archive.create(post: @post, user: current_user)
 		flash[:success] = "Like Counted!"
 		redirect_to @post
 	end

 	#좋아요 취소
 	def delete_archive
 		@archive = Archive.find_by(:user_id => current_user.id)
 		@archive.destroy
 		redirect_to @post
 	end

 
 	private
 
 	def post_params
 		params.require(:post).permit(:title,:content,:image,:category,:season)
 	end
 
 	def set_post
 		@post = Post.find(params[:id])
 	end
end
